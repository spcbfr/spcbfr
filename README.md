# Hello friend..
Hi, my name is Youssef, but you can call me spcbfr (prenounced as spacebuffer)

- 👨‍💻 I write code in `.js`, `.php`, `.py` and sometimes even `.elisp` 
- 💻 I am currently rocking a thinkpad t420 which is running arch linux and xmonad
- 📝 I maintain a [blog](https://spcbfr.vercel.app) where I write about geeky stuff, go check it out !
